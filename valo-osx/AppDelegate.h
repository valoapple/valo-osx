//
//  AppDelegate.h
//  valo-osx
//
//  Created by Mikhail Savchenko on 19.02.16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MainWindowController;
@class StatusItemView;

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (strong, nonatomic) StatusItemView *statusItem;
@property (strong, nonatomic) NSPopover *popover;
@property (strong, nonatomic) MainWindowController *controller;

@end

