//
//  LoginController.h
//  valo-osx
//
//  Created by Mikhail Savchenko on 19.02.16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface LoginController : NSViewController


@property (weak) IBOutlet NSButton *button;

@end
