//
//  LoginController.m
//  valo-osx
//
//  Created by Mikhail Savchenko on 19.02.16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import "LoginController.h"

@interface LoginController ()

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do view setup here.
    
    NSColor *color = [NSColor whiteColor];
    NSMutableAttributedString *colorTitle = [[NSMutableAttributedString alloc] initWithAttributedString:[_button attributedTitle]];
    NSRange titleRange = NSMakeRange(0, [colorTitle length]);
    [colorTitle addAttribute:NSForegroundColorAttributeName value:color range:titleRange];
    //[colorTitle addAttribute:NSBackgroundColorAttributeName value:color range:titleRange];
    [_button setAttributedTitle:colorTitle];
    [[_button layer] setBackgroundColor:[[NSColor redColor] CGColor]];

}

@end
