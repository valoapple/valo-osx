//
//  main.m
//  valo-osx
//
//  Created by Mikhail Savchenko on 19.02.16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
