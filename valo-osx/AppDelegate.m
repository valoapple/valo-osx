//
//  AppDelegate.m
//  valo-osx
//
//  Created by Mikhail Savchenko on 19.02.16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginController.h"
#import "MainWindowController.h"
#import "StatusItemView.h"

@interface AppDelegate ()


@end

@implementation AppDelegate




- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    
    NSStoryboard *storyboard = [NSStoryboard storyboardWithName:@"Main" bundle:nil];
    _controller = [storyboard instantiateControllerWithIdentifier:@"MainWindowController"];
    
    NSStatusItem *statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSSquareStatusItemLength];
    _statusItem = [[StatusItemView alloc] initWithStatusItem:statusItem];
    //statusItem.menu = ([VLSession sharedSession] ? [VLDProfileMenu menu] : [VLDLoginMenu menu]);
    //    if (NSClassFromString(@"NSStatusBarButton")) {
    //        statusItem.button.image = image;
    //    }
    //    else {
    _statusItem.image = [NSImage imageNamed:@"StatusIcon"];
    _statusItem.alternateImage = [NSImage imageNamed:@"StatusIcon"];
    [self.statusItem setTarget:self];
    [self.statusItem setAction:@selector(statusItemAction:)];
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "cim.valo_osx" in the user's Application Support directory.
    NSURL *appSupportURL = [[[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    return [appSupportURL URLByAppendingPathComponent:@"cim.valo_osx"];
}

- (void) statusItemAction:(id) sender {

    //self.statusItem.button.state = NSOnState;
    [self.statusItem resignFirstResponder];
    if(self.statusItem.isHighlighted == NO)
    {
    self.statusItem.isHighlighted = YES;
    
    if (self.popover == nil)
    {
        // create and setup our popover
        _popover = [[NSPopover alloc] init];
        
        
        // the popover retains us and we retain the popover,
        // we drop the popover whenever it is closed to avoid a cycle
        

        self.popover.contentViewController = _controller;
        
        // so we can be notified when the popover appears or closes
        self.popover.delegate = self;
        
    }

    _popover.behavior = NSPopoverBehaviorTransient;
    [_popover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
    
    }
    else {
        self.statusItem.isHighlighted = NO;
        //[self.popover close];
        
        
    }
    //NSLog(@"%@",[NSNumber numberWithInt:self.statusItem.button.state]);
}

- (void)popoverWillShow:(NSNotification *)notification
{
    NSPopover *popover = notification.object;
    if (popover != nil)
    {
        [_popover becomeFirstResponder];
    }
}
// -------------------------------------------------------------------------------
// Invoked on the delegate when the NSPopoverDidShowNotification notification is sent.
// This method will also be invoked on the popover.
// -------------------------------------------------------------------------------
- (void)popoverDidShow:(NSNotification *)notification
{
    [_popover becomeFirstResponder];
    // add new code here after the popover has been shown
}

// -------------------------------------------------------------------------------
// Invoked on the delegate when the NSPopoverWillCloseNotification notification is sent.
// This method will also be invoked on the popover.
// -------------------------------------------------------------------------------
- (void)popoverWillClose:(NSNotification *)notification
{
    NSString *closeReason = [notification.userInfo valueForKey:NSPopoverCloseReasonKey];
    if (closeReason)
    {
        // closeReason can be:
        //      NSPopoverCloseReasonStandard
        //      NSPopoverCloseReasonDetachToWindow
        //
        // add new code here if you want to respond "before" the popover closes
        //
    }
    self.statusItem.isHighlighted = NO;
}

/*- (void) statusItemAction:(id) sender
{
    // Get the frame and origin of the control of the current event
    // (= our NSStatusItem)
    CGRect eventFrame = [[[NSApp currentEvent] window] frame];
    CGPoint eventOrigin = eventFrame.origin;
    CGSize eventSize = eventFrame.size;
    
    // Create a window controller from your xib file
    // and get the window reference
    if(self.windowController == nil)
        self.windowController = [[MainWindowController alloc] initWithWindowNibName:@"MainWindowController"];
    NSWindow *window = [self.windowController window];
    
    // Calculate the position of the window to
    // place it centered below of the status item
    CGRect windowFrame = window.frame;
    CGSize windowSize = windowFrame.size;
    CGPoint windowTopLeftPosition = CGPointMake(eventOrigin.x + eventSize.width/2.f - windowSize.width/2.f, eventOrigin.y - 20);
    
    // Set position of the window and display it
    [window setFrameTopLeftPoint:windowTopLeftPosition];
    [window makeKeyAndOrderFront:self];
    
    // Show your window in front of all other apps
    [NSApp activateIgnoringOtherApps:YES];
    
    [self.statusItem setHighlightMode:YES];
}*/


@end
