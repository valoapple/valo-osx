//
//  ValoXPC.h
//  ValoXPC
//
//  Created by Mikhail Savchenko on 19.02.16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ValoXPCProtocol.h"

// This object implements the protocol which we have defined. It provides the actual behavior for the service. It is 'exported' by the service to make it available to the process hosting the service over an NSXPCConnection.
@interface ValoXPC : NSObject <ValoXPCProtocol>
@end
