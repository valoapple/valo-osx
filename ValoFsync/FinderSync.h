//
//  FinderSync.h
//  ValoFsync
//
//  Created by Mikhail Savchenko on 19.02.16.
//  Copyright © 2016 Mikhail Savchenko. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <FinderSync/FinderSync.h>

@interface FinderSync : FIFinderSync

@end
